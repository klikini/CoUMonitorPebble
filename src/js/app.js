// Pebble libraries
var UI = require("ui");
var ajax = require("ajax");
var menu;

// Start the app
load();

function load() {
	// Make a request
	ajax(
		// I don't count this as non-egyptian braces because it's a vertical list of functions
		{
			// Server status URL to request
			url: "https://server.childrenofur.com:8181/serverStatus",
			type: "json"
		},
		function(data) {
			// Request succeeded
			display(data);
		},
		function(error) {
			// Request did not succeed, something is wrong
			new UI.Card({
				title: "CoU Server",
				subtitle: "\nServer is down :("
			}).show();
		}
	);
	// Repeat in 5 seconds
    //ANNOYING setTimeout(load, 5000);
}

// Display the returned data
function display(data) {
	// Assemble the menu list
	var menuList = [
		{
			title: "Memory usage",
			subtitle: formatStorage(data.bytesUsed)
		},
		{
			title: "CPU usage",
			subtitle: data.cpuUsed + "%"
		},
		{
			title: "Uptime",
			subtitle: data.uptime
		}
	];
	
	// Assemble the player list in menu format
	var playerList = [];
	data.playerList.forEach(function(player) {
		playerList.push({
			title: player
		});
	});
	
	// Assemble the menu
	var newMenu = new UI.Menu({
		sections: [
			{
				title: "Server is up!",
				items: [{
					title: "Refresh",
					subtitle: "Select to refresh stats"
				}]
			},
			{
				title: "Stats (live server)",
				items: menuList
			},
			{
				title: "Players Online: " + data.playerList.length,
				items: playerList
			}
		]
	});
	
	// Set up the refresh button
	newMenu.on('select', function(e) {
		if (e.item.title == "Refresh") {
			load();
		}
	});
	
	// Open the menu
	if (typeof menu !== "undefined") {
		menu.hide();
	}
	menu = newMenu;
	menu.show();
}

// Convert bytes to formatted units
function formatStorage(storage) {
	if (storage < 1000) {
		return Math.round(storage) + " B";
	} else if (storage >= 1000 && storage < 1048576) {
		return Math.round(storage * 0.001) + " KB";
	} else if (storage >= 1048576 && storage < 1073741824) {
		return Math.round(storage * Math.pow(10, -6)) + " MB";
	} else if (storage >= 1073741824) {
		return Math.round(storage * Math.pow(10, -9)) + " GB";
	}
}
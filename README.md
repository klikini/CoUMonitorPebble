# CoUMonitorPebble

[Pebble](http://getpebble.com) watchapp to monitor CoU Server Status (live game server)

![](http://i.imgur.com/m1ZbMgQ.png)

*Created on [CloudPebble](https://cloudpebble.net/)*
